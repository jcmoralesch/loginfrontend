import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { UsuarioComponent } from './components/usuario/usuario/usuario.component';
import { LoginComponent } from './components/usuario/login/login.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormUsuarioComponent } from './components/usuario/form-usuario/form-usuario.component';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TokenInterceptor } from './model/service/interceptors/token.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    UsuarioComponent,
    LoginComponent,
    FormUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(
      {
        //positionClass:'top-left',
        closeButton:true
      }
    ),
    ToastContainerModule,
    BrowserAnimationsModule,
    
  ],
  providers: [{provide: HTTP_INTERCEPTORS,useClass:TokenInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
